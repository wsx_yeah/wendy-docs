---
sidebar_position: 1
---
html-basic

## 事件  
### 事件冒泡、捕获  
https://link.zhihu.com/?target=https%3A//juejin.cn/post/6844903834075021326  
### 介绍下浏览器事件委托  
https://zh.javascript.info/event-delegation  
### 实现一个自定义事件  
https://link.zhihu.com/?target=https%3A//developer.mozilla.org/zh-CN/docs/Web/Guide/Events/Creating_and_triggering_events  
## dom操作
https://link.zhihu.com/?target=https%3A//juejin.cn/post/6844903546136035342   
## JavaScript获取DOM元素位置和尺寸大小  
https://link.zhihu.com/?target=https%3A//www.cnblogs.com/dolphinX/archive/2012/11/19/2777756.html
