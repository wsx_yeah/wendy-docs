---
sidebar_position: 1
---

## 类型
https://link.zhihu.com/?target=https%3A//developer.mozilla.org/zh-CN/docs/Web/JavaScript/Data_structures
#### typeof  
https://link.zhihu.com/?target=https%3A//segmentfault.com/q/1010000011846328
#### 类型转换
https://link.zhihu.com/?target=https%3A//github.com/amandakelake/blog/issues/34


## 作用域  
https://link.zhihu.com/?target=https%3A//github.com/mqyqingfeng/Blog/issues/3  

#### 作用域链  
https://link.zhihu.com/?target=https%3A//github.com/mqyqingfeng/Blog/issues/6
#### 变量提升  
#### let、const、var  
https://link.zhihu.com/?target=https%3A//es6.ruanyifeng.com/%23docs/let%23%25E5%259D%2597%25E7%25BA%25A7%25E4%25BD%259C%25E7%2594%25A8%25E5%259F%259F

## 执行上下文  
https://link.zhihu.com/?target=https%3A//github.com/mqyqingfeng/Blog/issues/4
#### 上下文和执行栈  
https://link.zhihu.com/?target=https%3A//segmentfault.com/a/1190000018550118

## 闭包 
为什么需要闭包  
https://link.zhihu.com/?target=http%3A//blog.leapoahead.com/2015/09/15/js-closure/  
https://link.zhihu.com/?target=https%3A//github.com/lgwebdream/FE-Interview/issues/17  
https://link.zhihu.com/?target=https%3A//juejin.cn/post/6844903769646317576  
#### 原理  
#### 场景  
#### 闭包产生的问题、优化  
https://link.zhihu.com/?target=https%3A//blog.csdn.net/zheng_pika/article/details/79139050  
https://link.zhihu.com/?target=https%3A//github.com/raxxarr/note/issues/3

## this指向  
https://link.zhihu.com/?target=https%3A//www.cnblogs.com/tangjianqiang/p/13476908.html  
#### 改变this指向  
#### 箭头函数  
https://zhuanlan.zhihu.com/p/26540168  
#### 为什么需要箭头函数、为什么需要class（消除 function 对new 指令的二义性）  

## 原型/继承  
https://zhuanlan.zhihu.com/p/87667349
https://link.zhihu.com/?target=https%3A//www.jianshu.com/p/dee9f8b14771  
https://link.zhihu.com/?target=https%3A//www.jianshu.com/p/652991a67186  
https://link.zhihu.com/?target=https%3A//www.jianshu.com/p/a4e1e7b6f4f8  
#### 原型链图  
#### js如何实现继承  
https://link.zhihu.com/?target=https%3A//juejin.cn/post/6844904098941108232  
#### ES5、ES6的继承  
https://link.zhihu.com/?target=https%3A//github.com/Advanced-Frontend/Daily-Interview-Question/issues/20  
## 事件循环  
https://zhuanlan.zhihu.com/p/33087629  
https://link.zhihu.com/?target=https%3A//juejin.cn/post/6844903955286196237  
https://link.zhihu.com/?target=https%3A//juejin.cn/post/6844904035770695693  
#### 微任务、宏任务  
#### 浏览器和nodejs  
https://link.zhihu.com/?target=https%3A//juejin.cn/post/6844903761949753352  

## 异步编程  
#### 异步解决方案  
https://zhuanlan.zhihu.com/p/36739965
#### Promise、Promise A+ 规范
