#  计算机组成原理  
## 计算机组成概述
###  现代计算机组结构  
  
###  计算机的组成  
:::note  
1.输入设备，是指将外部信息以计算机能读懂的方式输入进来，如键盘，鼠标等  
2.输出设备，就是将计算机处理的信息以人所能接受的方式输出出来，比如显示屏，打印机。  
3.存储器，存储器分为 主存储器(内存储器，CPU能直接访问)和 辅助存储器(外存储器，协助主存储器记忆更多的信息，辅助存储器的信息需要导入到主存储器中，才可以被CPU访问)。  
4.运算器，是计算机的运算单元，用于算术运算和逻辑运算，运算器的核心单元是算术逻辑单元(ALU)。  
5.控制器，控制器是计算机的指挥中心，有其指挥各部件自动协调第进行工作，现代计算机将运算器和控制器集成到一个芯片上，合成为中央处理器，简称CPU。有程序计数器(PC)、指令寄存器(IR)和控制单元(CU)。
:::



import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
  <TabItem value="存储器" label="存储器" default>
:::tip  
主存储器是由地址寄存器(MAR)，数据寄存器(MDR)，存储体，时序控制逻辑组成
:::
  </TabItem>
  <TabItem value="运算器" label="运算器">
:::tip  
用于实现算数运算、逻辑运算  
ACC: 累积器, 用于存放操作数, 或运算结果  
MQ: 乘商寄存器,在乘 除运算时存放操作数或结果
X: 通用的操作数寄存器,用于存放操作数  
ALU: 算数逻辑单元 通过内部复杂的电路实现算数运算、逻辑运算
:::
  </TabItem>
  <TabItem value="控制器" label="控制器">
:::tip
有程序计数器(PC)、指令寄存器(IR)和控制单元(CU)。
:::
  </TabItem>
</Tabs>




