// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'My Site',
  tagline: 'Dinosaurs are cool',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://wsx_yeah.gitee.io',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/wendy-docs/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: '', // Usually your GitHub org/user name.
  projectName: 'wendy-docs', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: '前端进阶笔记',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'Tutorial',
          },
          {
            type: 'dropdown',
            label: '前端基础',
            position: 'left',
            items: [
              {
                type: 'docSidebar',
                sidebarId: 'frontHtmlSidebar',
                label: 'html',
              },
              {
                type: 'docSidebar',
                sidebarId: 'frontJsSidebar',
                label: 'js',
              },
              {
                type: 'docSidebar',
                sidebarId: 'frontCssSidebar',
                label: 'css',
              },
              {
                type: 'docSidebar',
                sidebarId: 'browserSidebar',
                label: '浏览器原理',
              },
              {
                type: 'docSidebar',
                sidebarId: 'taskSidebar',
                label: '任务队列',
              },
              {
                type: 'docSidebar',
                sidebarId: 'eventSidebar',
                label: '事件委托',
              },
              // ... more items
            ],
          },
          {
            type: 'dropdown',
            label: '前端框架',
            position: 'left',
            items: [
            ],
          },
          {
            type: 'dropdown',
            label: '工程化',
            position: 'left',
            items: [
            ],
          },
          {
            type: 'dropdown',
            label: '架构',
            position: 'left',
            items: [
              {
                type: 'docSidebar',
                sidebarId: 'OSSSidebar',
                label: '单点登录',
              },
            ],
          },
          {
            type: 'dropdown',
            label: '中后台模板',
            position: 'left',
            items: [
            ],
          },
          {
            type: 'dropdown',
            label: '计算机基础',
            position: 'left',
            items: [
              {
                type: 'docSidebar',
                sidebarId: 'itBasicSidebar',
                label: '常识',
              },
              {
                type: 'docSidebar',
                sidebarId: 'COSidebar',
                label: '计算机组成原理',
              }
              // ... more items
            ],
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            type: 'search',
            position: 'left',
          },
          {
            href: 'https://gitee.com/wsx_yeah/wendy-docs',
            label: 'Gitee',
            position: 'right',
          },
        ],
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),
};

export default config;
